## The Shortcut x Futurice JS Coding Club

[Futurice](https://futurice.com/)
[The Shortcut](https://theshortcut.org/)

### Not Another To-Do List App

The August sessions of The Shortcut's JavaScript Club will focus on creating a task/project management tool with Javascript! 

You'll be tasked to build a task management tool for yourself. Based on your current skill level, it can vary anywhere between a todo list to full-blown project management system so it's a great challenge for anyone regardless of the level of your starting knowledge. And it's open-ended so you can use your creativeness to decide which direction to go.

My goals for this project are to strengthen my skills with MERN technologies, and to focus on learning [GitLab's CI/CD](https://docs.gitlab.com/ee/ci/README.html) tool for software development

### Programming Stack
- MondoDb
- Express
- React
- Nodejs

### IT Technologies
- Gitlab CI/CD
- Heroku

### Weekly Plan

* Week 1: Design, Prep, Demo
    * Goal: Basic To Do List funcitonality
        * CRUD functions
        * CI/CD Pipeline setup
* Week 2: Database integration
    * Goal: Expanding functionality
        * Grouping ordered lists
* Week 3: Responsive Design
* Week 4: Polish